# Week 20; Monday May 17th 2021  
3rd week of the month;
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday May 24th 2021](/minutes/2021-05-24),  
Prevous meeting [Monday May 10th 2021](/minutes/2021-05-10)  
  
Original record [office://qBiLMBTKdLaXoQqgdzxN2b](https://office.communitycoins.net/grain/qBiLMBTKdLaXoQqgdzxN2b/)  

## In attendance  
- Gerrit
- Jeff Bouchard
- koad

## Proposed subjects  
- Wrap-up Auroracoin raid
- How to better protect reputation of communitycoins
- Should we be selective? "We can not take all responsibility" versus "eventually every community will - become mature"
- Bitcoin is proven but can we blame instability to immaturity?  and who is to blame?
	- There is much more to stability besides mining (Only mining is being rewarded, but that leaves many fragilities)
	- Enhance 51% security
	- Maintain Pools
	- Maintain loyal mining (irrespective of optimized profitibility)
	- Maintain market access
	- Maintain electrumservers --- 
	- Maintain helpdesk
	- Education
- Elaborate functionality and lowering of threshold
	- Shilling, marketing, liquidity???
	- Collaboration on vital technologies (Electrum, atomicdex, Bitcoin...)
	- https://bitcointalk.org/index.php?topic=5330827.msg57014309#msg57014309 (ptc blinkered)
- Pertinence of a cooperation in maintaining a shared Ethereum full archival node for lower cost bridge/gateway and other monetizations
Ness leading notwithstanding, we can join an existing effort if partners are proven trustworthy enough. definition to be settled between us
