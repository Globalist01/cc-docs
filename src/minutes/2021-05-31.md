

# Week 22; Monday May 31st 2021  
5th week of the month; Casual meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday June 7th 2021](/minutes/2021-06-07),  
Prevous meeting [Monday May 24th 2021](/minutes/2021-05-24)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


