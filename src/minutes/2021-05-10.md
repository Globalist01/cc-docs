

# Week 19; Monday May 10th 2021  
2nd week of the month; Core/Executive meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday May 17th 2021](/minutes/2021-05-17),  
Prevous meeting [Monday May 3rd 2021](/minutes/2021-05-03)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


