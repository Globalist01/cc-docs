

# Week 26; Monday June 28th 2021  
4th week of the month; Core/Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday July 5th 2021](/minutes/2021-07-05),  
Prevous meeting [Monday June 21st 2021](/minutes/2021-06-21)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


