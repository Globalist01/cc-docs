

# Week 37; Monday September 13th 2021  
2nd week of the month; Core/Executive meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday September 20th 2021](/minutes/2021-09-20),  
Prevous meeting [Monday September 6th 2021](/minutes/2021-09-06)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


