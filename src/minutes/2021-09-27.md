

# Week 39; Monday September 27th 2021  
4th week of the month; Core/Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday October 4th 2021](/minutes/2021-10-04),  
Prevous meeting [Monday September 20th 2021](/minutes/2021-09-20)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


