## Technologies used within these works

# definate

## Meteor
Meteor is an open source framework for seamlessly building and deploying Web, Mobile, and Desktop applications in Javascript.  It's build tools are heavely used within koad:io.

```bash
npm install -g meteor
```
## Nodejs via NVM


```bash
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
```
reference: [github/nvm-sh/nvm](https://github.com/nvm-sh/nvm)

## Material for [MkDocs](https://www.mkdocs.org/)

source: [github/squidfunk/mkdocs-material](https://github.com/squidfunk/mkdocs-material)

# others

## Sandstorm


## mkDocs
https://www.mkdocs.org/

## mkDocs Material
https://github.com/squidfunk/mkdocs-material
https://squidfunk.github.io/mkdocs-material/setup/changing-the-fonts/


