# Quick Links

## Fun
koad's live stream -> [http://koad.live](http://koad.live)  
koad's custom radio player -> [http://audio.koad.xyz](http://audio.koad.live)  

## Workers
[http://tokel.daemoncoins.com/workers/RLteGDnSGz7HzvHN6JqBee7pkYFt8BZWDZ](http://tokel.daemoncoins.com/workers/RLteGDnSGz7HzvHN6JqBee7pkYFt8BZWDZ)

## Entities
Alice [https://alice.koad.xyz](https://alice.koad.xyz)  
Astro [https://astro.koad.xyz](https://astro.koad.xyz)  
Seraph [https://seraph.koad.xyz](https://seraph.koad.xyz) [https://seraph.ws](https://seraph.ws)  
Pandora [https://pandora.koad.xyz](https://pandora.koad.xyz) [https://pandora.ws](https://pandora.ws)  

## Brands
The Canada eCoin Project [https://canadaecoin.ca](https://canadaecoin.ca)  
The Church of Happy [https://churchofhappy.com](https://churchofhappy.com)  
GigaGeek Industries LTD [https://gigageek.ca](https://gigageek.ca)  
Zvaniga Family Services [https://zvaniga.com](https://zvaniga.com)  
eCoinCore [https://ecoincore.com](https://ecoincore.com)  
koad [https://koad.omg.lol](https://koad.omg.lol)  
The King of All Data [https://kingofalldata.com](https://kingofalldata.com)  
