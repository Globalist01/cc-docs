return console.log('yeah, .. dont run this shit.. it will over-write things..');

const fs = require('fs');

const days = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
const months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];
const meetingTypes = [ '', 
	'Development',
	'Core/Executive',
	'Development',
	'Core/Development',
	'Casual',
];

const englishOrdinal = [ "zero",
	"first", "second", "third", "fourth", "fifth", "sixth",
	"seventh", "eighth", "nineth", "tenth", "eleventh", "twelfth",
	"thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth", 
	"eighteenth", "nineteenth", "twentieth", "twenty-first", "twenty-second",
	"twenty-third", "twenty-fourth", "twenty-fifth", "twenty-sixth", 
	"twenty-seventh",	"twenty-eighth", "twenty-nineth", 
	"thirtieth", "thirty-first"
]

function getGetOrdinal(n) {
	var s=["th","st","nd","rd"], v=n%100;
	return n+(s[(v-20)%10]||s[v]||s[0]);
};

function getDateEnglishFormat(d) {

	const monthName = months[d.getMonth()]
	const dayIndex = d.getDay()
	const dayName = days[dayIndex] // Thu
	const dateIndex = d.getDate()
	const year = d.getFullYear();

	return `${dayName} ${monthName} ${getGetOrdinal(dateIndex)} ${year}`;
};

// Returns the ISO week of the date.
function getWeekNumber(d) {
	var date = new Date(d.getTime());
	date.setHours(0, 0, 0, 0);
	// Thursday in current week decides the year.
	date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
	// January 4 is always in week 1.
	var week1 = new Date(date.getFullYear(), 0, 4);
	// Adjust to Thursday in week 1 and count number of weeks from date to week1.
	return 1 + Math.round((( date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
}

function getPaddedTwoDigits(n) {
	let padding = String("0"+ n);
	return padding.slice(padding.length-2,padding.length);
};


function getFilenameFromDate(d) {
	return `/minutes/${d.getFullYear()}-${getPaddedTwoDigits(d.getMonth()+1)}-${getPaddedTwoDigits(d.getDate())}`;
};


// var now = new Date(2021, 10, 1);
var d = new Date(2021, 10, 1);
for (let step = 0; step < 55; step++) {
	console.log(`---`);

	const monthName = months[d.getMonth()]
	const dateIndex = d.getDate()
	const dayIndex = d.getDay()
	const dayName = days[dayIndex] // Thu

	const year = d.getFullYear();

	let paddedMonth = getPaddedTwoDigits(d.getMonth()+1);
	let paddedDate = getPaddedTwoDigits(d.getDate());

	const filename = getFilenameFromDate(d);

	// console.log(d.toString());
	const title = `${dayName} ${monthName} ${getGetOrdinal(dateIndex)} ${year}`;

	const weekIndex = Math.floor(Number(dateIndex -1 ) / 7) + 1
	const weekIndexOrdinal = getGetOrdinal(weekIndex)

	console.log(`${getDateEnglishFormat(d)};`);
	console.log(`${weekIndexOrdinal} week of the month; ${meetingTypes[weekIndex]} meeting; Week ${getWeekNumber(d)}`);

	const suroundingMeetings = new Date(d);

	suroundingMeetings.setDate(suroundingMeetings.getDate()+7);
	const nextMeeting = new Date(suroundingMeetings);

	suroundingMeetings.setDate(suroundingMeetings.getDate()-14);
	const previousMeeting = new Date(suroundingMeetings);




	const fileContent = `

# Week ${getWeekNumber(d)}; ${title}  
${weekIndexOrdinal} week of the month; ${meetingTypes[weekIndex]} meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [${getDateEnglishFormat(nextMeeting)}](${getFilenameFromDate(nextMeeting)}),  
Prevous meeting [${getDateEnglishFormat(previousMeeting)}](${getFilenameFromDate(previousMeeting)})

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


`


	fs.writeFile( `src${filename}.md`, fileContent, function (err) { if (err) throw err } );
	d.setDate(d.getDate()-7);

};

// console.log("-----")
// now = new Date(2021, 10, 1);
// for (let step = 0; step < 36; step++) {

// 	const monthName = months[d.getMonth()]
// 	const dateIndex = d.getDate()
// 	const dayIndex = d.getDay()
// 	const dayName = days[dayIndex] // Thu
// 	const year = 1900+d.getYear();

// 	let paddedMonth = String("0"+ (d.getMonth()+1));
// 	paddedMonth = paddedMonth.slice(paddedMonth.length-2,paddedMonth.length);

// 	let paddedDate = String("0"+ d.getDate());
// 	paddedDate = paddedDate.slice(paddedDate.length-2,paddedDate.length);

// 	const filename = `${year}-${paddedMonth}-${paddedDate}`;

// 	// console.log(d.toString());
// 	// console.log(`${filename}.md -> ${dayName} ${monthName} ${getGetOrdinal(dateIndex)} ${year}`);
// 	console.log(`[${dayName} ${monthName} ${getGetOrdinal(dateIndex)} ${year}](/minutes/${filename})`);

// 	d.setDate(d.getDate()-7);

// };